# Ruley

A very simple rule engine.


## Simple rules

Simple rules are defined by an object with a number of fields. If each of the fields matches the given context, then the rule passes.

This rule will pass if the context includes a name of "Fred" and a state equal to "MA".

```
{
    name: "Fred",
    state: "MA
}

```

## Matching multiple options

A rule with an array for the value of one of the fields will match if the value from the context matches any of the given values.

This rule will match if the context's name has either "Fred" or "Ted"

```
{
    name: ["Fred", "Ted"]
}

```

## "OR" rules

If the passed in rule object is an array, then the rule will pass if any of the child rules in the array pass.

In this example, the rule passes if the name is Fred and state is MA, or if name is "Ted" and state is "NH":

```
[
    {
        name: "Fred",
        state: "MA"
    },
    {
        name: "Ted",
        state: "NH"
    }
]
```